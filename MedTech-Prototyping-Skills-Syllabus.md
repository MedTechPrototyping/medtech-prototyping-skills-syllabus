# MedTech Prototyping Skills (BME254L) Syllabus

## Personnel

### Instructor

Dr. Mark Palmeri 

* [Teams](https://teams.microsoft.com) chat is the best way to reach me
* Email (slower): mark.palmeri@duke.edu
* Duke GitLab username: [mlp6](https://gitlab.oit.duke.edu/mlp6)
* Office: 258 Hudson Hall Annex
* Office Hours: https://calendly.com/mark-palmeri

### Teaching Assistants

* Darcy Ayers (`dra39`)
* Carson Pazdan (`cbp31`)
* Lauren Kenselaar (`lbk18`)

*Questions that can be answered by Dr. Palmeri or a teaching assistant should be
posted on Ed Discussion.*

## Course Times & Locations

**Lecture:** Tuesday & Thursday, 11:45-13:00, Hudson Hall 208 (Panopto recorded)

**Lab:** Thursday, 13:25-16:25; Fitzpatrick B209 (Door Code: 4-1-5-2)

⚠️ No food or drink is allowed in the lab! ⚠️  Failure to adhere to this policy will have consequences on your lab participation.

## Course Objectives

This course focuses on developing medical device prototyping skills that will be used in future project and design courses, with a focus on preparing our students for positions in the medtech industry.  Students will work individually to complete design tasks that will be tested to quantitative specifications.  Students will gain hands-on experience with device fabrication, debugging, testing and failure analysis.

Upon completion of this course, students should be able to:

* Software version control (`git`)
* Perform functional decomposition and express as a stage diagram (UML)
* Utilize ECAD (`KiCad`) for:
  * Electronic schematic capture
  * Printed circuit board (PCB) layout
* Modular breadboarding of circuits to translation to testable PCBs
* Design a battery-powered device that:
  * Accepts analog and digital user input
  * Outputs analog and digital outputs
  * Passes testable specifications to 95\% CI
* Implement electronic logic on microcontroller (Arduino framework)
  * Modular / testable code development in C
  * Interupt Service Routines
  * Pulse Width Modulation
  * State Machine
* Utilize CAD (`Onshape`) for:
  * Device enclosure design with UI/UX considerations
  * Input / Output considerations
  * Size / weight constraints
  * Preparation of mechanical drawings
* 3D printing of designs
  * Assembly of discrete enclosure pieces
  * Use of mechanical fasteners
* Write technical analysis reports

## Prerequisites

### Mandatory

* Computational Methods in Engineering (EGR103) or equivalent
* Fundamentals of Electrical and Computer Engineering (ECE110L)

### Recommended / Corequisite

* Introduction to Electricity, Magnetism and Optics (PHY152L)

## Learning Management System

We will be using [Canvas](https://canvas.duke.edu) as the learning management
system for this course.  It will host the syllabus, which will have hyperlinks
to all lecture content and lab assignments.

Duke's [GitLab](https://gitlab.oit.duke.edu) server will be used for most course
lab exercises, and code-related questions will be submitted to Dr. Palmeri / TAs
using GitLab Issues.

Ed Discussion will be used for general course questions and discussion.

## Class Schedule

This class is organized in a sequence of modules.  Specific details surrounding
dates for assignments associated with each module will be posted to Gradescope.

This course uses a version of [Mastery
Learning](https://en.wikipedia.org/wiki/Mastery_learning), where "mastery" of a
given module is necessary to progress onto the subsequent module.  Quizzes are
used to evaluate "knowledge"; lab exercises are used to demonstrate application
of skills.  In this course, assignments of later modules depends on the
successful completion of earlier modules.

### Learning Modules

| Module | Gradescope | Lab |
| --- | --- | --- |
| [FDOC: Who am I?](https://mlp6.pages.oit.duke.edu/FDOC_WhoAmI/FDOC_WhoAmI.html) / [Skill Overview](https://medtechprototyping.pages.oit.duke.edu/medtech-skill-overview/MedTech-Skill-Overview.html) | [Completion Survey](https://www.gradescope.com/courses/941961/assignments/5544754) | [Software Installation & Tutorials](https://gitlab.oit.duke.edu/MedTechPrototyping/medtech-skill-overview/-/blob/main/lab/Software-Install-Tutorials.md?ref_type=heads) |
| [Event-Driven State Machines](https://medtechprototyping.pages.oit.duke.edu/event-driven-state-machine/EventDrivenStateMachine.html) | [Wireless HRM State Diagram](https://www.gradescope.com/courses/941961/assignments/5582693) | [Wireless HRM State Diagram](https://gitlab.oit.duke.edu/MedTechPrototyping/event-driven-state-machine/-/blob/main/lab/EventDriveStateMachineLab.md?ref_type=heads) |
| [Version Control (`git`)](https://embeddedmedicaldevices.pages.oit.duke.edu/git-fundamentals/git-fundamentals.html) | [Git Fundamentals Quiz](https://www.gradescope.com/courses/941961/assignments/5637237) | [Git Lab Exercise](https://gitlab.oit.duke.edu/kits/BME-254L-001-Sp25/git-fundamentals-lab) |
| [Computer Aided Design (CAD) (`Onshape`)](https://medtechprototyping.pages.oit.duke.edu/cad-onshape/cad-onshape.html) | [Hybrid III Drawings & Screenshots](https://www.gradescope.com/courses/941961/assignments/5674613) | [Hybrid III 6 y/o](https://gitlab.oit.duke.edu/MedTechPrototyping/cad-onshape/-/blob/main/lab/hybrid_iii_6yo.md?ref_type=heads) |
| [ECAD (`KiCad`): Schematic Capture](https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-Schematic-Capture.html) | None | [Schematic Capture: Voltage Dividers and LED Output](https://gitlab.oit.duke.edu/kits/BME-254L-001-Sp25/kicad-schematic-spice-lab/-/blob/main/README.md?ref_type=heads) |
| [ECAD (`KiCad`): SPICE Modeling](https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-Spice-Modeling.html) | None | [SPICE Modeling Lab](https://gitlab.oit.duke.edu/kits/BME-254L-001-Sp25/kicad-schematic-spice-lab/-/blob/main/README.md?ref_type=heads) |
| [ECAD (`KiCad`): PCB Layout](https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-PCB-Layout.html) | None | [PCB Layout Lab: Microcontroller Peripherals](https://gitlab.oit.duke.edu/kits/BME-254L-001-Sp25/kicad-schematic-spice-lab/-/blob/main/README.md?ref_type=heads) |
| Firmware Development: C Programming Overview | None | [PlatformIO / Arduino Framework: Timing & GPIO](https://gitlab.oit.duke.edu/kits/BME-254L-001-Sp25/arduino_timing_gpio) (Paired Coding)|
| Firmware Development: Button Event ISRs, ADC & State Machines | None | Button Event ISR & ADC (Paired Coding) |
| Lightbox Project | None | Lab Work Time |
| Breadboard Testing / Electronics Bench Equipment | None | Lab Work Time |
| PCB Fabrication / Soldering | None | Lab Work Time |
| Enclosure Design / 3D Printing | None | Lab Work Time |
| Integration & Testing | None | Lab Work Time |
| Final Demo & Technical Report | Technical Report | In-Person Presentation |
<!--
| [Firmware Development: C Programming Overview](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/c-programming/-/blob/main/lecture/C-programming.md?ref_type=heads) | Coming Soon | [PlatformIO / Arduino Framework: Timing & GPIO](https://gitlab.oit.duke.edu/kits/BME-290L-002-F24/arduino_timing_gpio) |
| [Firmware Development: Button Event ISRs, ADC & State Machines](https://gitlab.oit.duke.edu/MedTechPrototyping/arduino_isr_adc/-/blob/main/src/main.cpp) | None | [Button Event ISR & ADC](https://gitlab.oit.duke.edu/MedTechPrototyping/arduino_isr_adc/-/blob/main/README.md) |
| [Lightbox Project](https://gitlab.oit.duke.edu/kits/BME-290L-002-F24/lightbox/-/blob/main/README.md) | None | On Your Schedule |
-->

### Special Dates

* MLK Holiday (Jan 20, 2025)
* Spring Break (Mar 10-14, 2025)
* LDOC (Apr 23, 2025)

## Attendance & Participation

Class participation in both lecture and lab time is strongly encouraged.  Lecture will be used to provide skill overview and live demonstrations, many of while will kickstart your efforts for your project.  Lab time will provide you access to equipment and the TAs for assistance.

Students are responsible for obtaining missed lecture content from other students in the class.  All lecture slides/presented content will be made availabe online (Canvas/Gitlab), and lectures will be recorded via Panopto and posted to Canvas.

Participation on Ed Discussion is also encouraged, in the form of:

* Asking questions about the course material (ideally, publicly, so that others * can benefit (Anonymous okay))
* Answering questions from other students
* Sharing interesting articles or resources related to the course material

## Assignments & Grading

### Grading

There will be quizzes, lab exercises, and completion surveys associated with some of the learning modules (25%), all of which will be submitted through Gradescope.  All of the learning modules will contribute to the final project and the associated technical report (75%).

All assignment grades will be posted to Gradescope (and linked to the Canvas gradebook) throughout the semester to track your performance.

### Course Grade

This course is not "curved" (i.e., a distribution of grades will not be enforced), and a traditional grading scheme will be used (e.g., 90-93 = A-, 94-97 = A, 97-100 = A+).  Participation throughout the semester will influence rounding up/down for fractional grades.

Failing the course can happen with a cummulative score < 65 (D) or not completing all of the assignments.  

### Regrades

Any regrading requests need to be made **within one week of grades for a given
assignment being released**. You must make the request via Gradescope and
provide a description of why you feel a regrade is appropriate. Requesting a
regrade could lead to additional loss of credit when an assignment is
re-evaluated.

Some assignments will have an opportunity to be resubmitted based on grading
feedback at the discretion of Dr. Palmeri.

### Late Policy / SDAO Accomodations

Late submission windows will be available for all assignments, minus the final project, and should be used to accomodate acute illness, travel, high workload from other classes and other unforeseen circumstances.  This late submission window can be utilized without penalty and without prior approval.

Students with SDAO accomodations for extended time on assignments can use this extended late submission window for all assignments.

Any assignments submitted after the late submission window will only be accepted for partial credit at the discretion of Dr. Palmeri or if prior approval was sought **before the original due date**.

## Duke Community Standard

All students are expected to adhere to all principles of the [Duke Community Standard](https://trinity.duke.edu/undergraduate/academic-policies/community-standard-student-conduct). Violations of the Duke Community Standard will be referred immediately to the Office of Student Conduct. Please do not hesitate to talk with Dr. Palmeri about any situations involving academic honor, especially if it is ambiguous what should be done.

## FAQ

### Can I collaborate with other students?

Engineering is inherently a collaborative field, and in this class, you are encouraged to work collaboratively on your projects.  That being said, all of the work that you submit must be generated by you and reflect your understanding of the material. **All resources used in your projects that were developed by another person or company must be properly acknowledged using comments in your code / reports.**

### Can I use AI?

The use of artificial intelligence is a rapidly developing resource / tool in engineering.  In software development, there are many levels of AI-assitance available.  Such form of assistance include the [IntelliCode](https://visualstudio.microsoft.com/services/intellicode/) tools and [GitHub CoPilot](https://github.com/features/copilot) (free to students through the [GitHub Education](https://github.com/education/students) program).  These tools can be leveraged to help with syntax.  **You are, however, strongly cautioned to not rely on these tools for logical implementation.**

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.oit.duke.edu/mlp6/MedTech-Prototyping-Skills/">MedTech Prototyping Skills</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://bme.duke.edu/faculty/mark-palmeri">Mark L. Palmeri</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>
